# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :shopeecrawl,
  ecto_repos: [Shopeecrawl.Repo]

# Configures the endpoint
config :shopeecrawl, ShopeecrawlWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8OjmRKjJ82QzDkNTJXvCsmwN2wWmPnDBPR8uorY9R5XI57inV34PRYNXfREXH278",
  render_errors: [view: ShopeecrawlWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Shopeecrawl.PubSub,
  live_view: [signing_salt: "3scD7gPn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
