defmodule Shopeecrawl.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def change do
    create table(:categories) do
      add :name, :string
      add :products, :text

      timestamps()
    end

  end
end
