defmodule Shopeecrawl.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :price, :integer
      add :original_price, :integer
      add :image_id, :string, null: true

      timestamps()
    end

  end
end
