const express = require("express");
const bodyParser = require("body-parser");
var morgan = require("morgan");
const puppeteer = require("puppeteer");

const server = express();
server.use(bodyParser.json());
server.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);

function getData(url, match) {
  return new Promise(async (resolve, reject) => {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox", "--disable-setuid-sandbox"],
    });
    const page = await browser.newPage();
    page.on("response", async (response) => {
      if (response.url().includes("shop_categoryids"))
        console.log(response.url());

      if (response.url().includes(match)) {
        const data = await response.json();
        resolve(data);
      }
    });
    await page.goto("https://shopee.vn/apple_flagship_store", {
      waitUntil: "networkidle0",
    });
    await browser.close();
  });
}

server.post("/", async function (req, res) {
  console.log(req.body);
  const { url, match } = req.body;

  if (!url || !match)
    return res.status(400).json({ message: "Missing `url` or `match` " });

  const data = await getData(url, match);

  return res.json(data);
});

server.listen(4001, function () {
  console.log("server listening at 4001");
});
