// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss";

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html";

import axios from "axios";

(function () {
  const loading = document.getElementById("loading");
  const asyncButton = document.getElementById("async-button");
  asyncButton.addEventListener("click", function () {
    const url = "/api/fetch";
    fetch(url).then(() => alert("Fetch request sent to server"));
    this.setAttribute("disabled", true);
  });

  const syncButton = document.getElementById("sync-button");

  syncButton.addEventListener("click", function () {
    const url = "/api/fetch?async=N";
    loading.classList.add("active");
    axios
      .get(url)
      .then(() => {
        window.location.href = "/";
      })
      .catch((error) => {
        alert("Error occur!!! Please try again later");
      })
      .finally(() => loading.classList.remove("active"));
  });
})();
