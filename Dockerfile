FROM elixir:latest

WORKDIR /app

RUN apt-get update \
  && apt-get install -y wget \
  && mix local.hex --force \
  && mix archive.install --force hex phx_new 1.5.8 \
  && apt-get update --fix-missing \
  && curl -sL https://deb.nodesource.com/setup_12.x | bash \
  && apt-get install -y apt-utils \
  && apt-get install -y nodejs \
  && apt-get install -y build-essential \
  && apt-get install -y inotify-tools \
  && mix local.rebar --force \
  && npm install -g nodemon \
  && apt-get install -y wget gnupg \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
   --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

COPY ./assets/package*.json ./assets/

COPY mix.exs .

COPY mix.lock .

RUN mix deps.get --only prod && npm install --prefix ./assets


COPY . .

ENV SECRET_KEY_BASE "HK7wGrXkXen0tdNkyNuB9GYQj7uirHAuGfHS/eSqrNSIwf6amz8tBEzTW2ouLWVl"

# In real world, we will store this info in environment
ENV DATABASE_URL "ecto://kylo:abc@123123@35.247.159.124/kylodb"

ENV MIX_ENV "prod"

ENV PORT 4002

RUN mix compile && npm run deploy --prefix ./assets && mix phx.digest && mix ecto.migrate

EXPOSE 4002


CMD ["sh","-c","mix phx.server ; npm run proxy --prefix ./assets &"]
