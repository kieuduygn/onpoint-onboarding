defmodule ShopeecrawlWeb.PageController do
  use ShopeecrawlWeb, :controller

  alias Shopeecrawl.{Repo, Product, Category}
  import Ecto.Query

  @doc """
  The category page (it will not have the `category` in query string)
  """
  def index(conn, %{"category" => category}) do
    category_id = String.to_integer(category)
    IO.inspect category_id
    case Repo.get(Category, category_id) do
      nil -> render(conn, "error.html")
      category ->
        # Getting product_ids
        product_ids =
        String.split(category.products, "|")
        |> Enum.map(fn x -> String.to_integer(x) end)

        # Fetch products
        products = Product |> where([p], p.id in ^product_ids) |> Repo.all
        render(conn, "index.html", products: products)
      end
    end


  @doc """
  The home page (it will not have the `category` in query string)
  """
  def index(conn, _params) do
    products = Repo.all(Product)
    render(conn, "index.html", products: products)
  end


  defp fetch_sync(conn) do

    # result = Spider.fetch_all(false)
    # IO.puts "----------------------------------------"
    # IO.inspect result
    # IO.puts "----------------------------------------"
    try do
      result = Spider.fetch_all(false)
      json(conn, %{success: true})
    rescue
      RuntimeError -> conn |> put_status(500) |> json(%{success: false, message: "Server error"})
    end
  end


  @doc """
  The fetch data
  """
  def fetch(conn, %{"async" => async}) do
    IO.inspect async
    case async do
      # "Y" -> ShopeecrawlWeb.PageController.fetch(conn)
      "N" -> fetch_sync(conn)
    end
  end

  @doc """
  Default fetch async
  """
  def fetch(conn, _params) do
    Spider.fetch_all(true)
    json(conn, %{success: true})
  end
end
