defmodule ShopeecrawlWeb.LayoutView do
  use ShopeecrawlWeb, :view

  alias Shopeecrawl.{Repo, Category}

  @doc """
  Fetching data
  """
  def categories() do
    try do
      Repo.all(Category)
    rescue
      RuntimeError -> []
    end
  end
end
