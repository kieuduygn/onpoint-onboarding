defmodule Shopeecrawl.Repo do
  use Ecto.Repo,
    otp_app: :shopeecrawl,
    adapter: Ecto.Adapters.Postgres
end
