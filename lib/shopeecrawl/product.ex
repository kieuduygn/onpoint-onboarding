defmodule Shopeecrawl.Product do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products" do
    field :image_id, :string
    field :name, :string
    field :original_price, :integer
    field :price, :integer

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :price, :original_price, :image_id])
    |> validate_required([:name, :price, :original_price, :image_id])
  end
end
