defmodule Spider do
  @moduledoc """
  This module use to fetch data from Shopee
  """

  alias Shopeecrawl.{Product, Category, Repo}

  # NodeJs proxy url
  @proxy_url "http://localhost:4001"


  # The shop id
  @shop_id "88201679"


  # Shop url
  @shop_url "https://shopee.vn/apple_flagship_store"



  @doc """
  The http client
  """
  def fetch(url, match) do

    headers = [
      Accept: "Application/json; Charset=utf-8",
      "Content-Type": "Application/json; charset=utf-8",
      ]
    payload = %{"url" => url, "match" => match}
    HTTPoison.post!(@proxy_url, Jason.encode!(payload), headers, [recv_timeout: 300000])
  end


  @doc """
  Fetchng all product (use for index page)

  Currently Shopee does not allow fetching data from their site (they response wrong information).
  In additionally, their site is also a SPA, we cannot parse html data. We need to use a headless
  browser to crawl

  I currently cannot find any good headless browser in elixir, so I run another server (NodeJs) which
  I named proxy, this server does only one task

  1. Go to appropriate site
  2. Response the json data from Shopee
  """
  def get_products() do
    try do
      %HTTPoison.Response{body: body} = fetch(
        @shop_url,
         "?by=pop&limit=30&match_id=#{@shop_id}&newest=0&order=desc&page_type=shop&version=2")
      json = Jason.decode!(body)

      # Remove all producgts
      Repo.delete_all(Product)

      # Setup new list to insert
      entries = json["items"] |> Enum.map(fn item -> [
        name: item["name"],
        price: div(item["price"], 100000),
        original_price: div(item["price_before_discount"], 100000),
        image_id: item["image"],
        inserted_at: NaiveDateTime.local_now,
        updated_at: NaiveDateTime.local_now,
        id: item["itemid"]
       ] end)

      # Insert new products
      Repo.insert_all(Product, entries)
    rescue
      HTTPoison.Error -> raise "Failed to fetch"
    end

  end

  @doc """
  Fetchng all categories
  """
  def get_categories(async \\ true) do
    try do
      %HTTPoison.Response{body: body} = HTTPoison.get!(
        "https://shopee.vn/api/v2/shop/get_categories?limit=20&offset=0&shopid=#{@shop_id}"
      )
      json = Jason.decode!(body)
      Repo.delete_all(Category)

      # Setup new categories to insert
      entries = json["data"]["items"] |> Enum.map(fn item -> [
        name: item["name"],
        inserted_at: NaiveDateTime.local_now,
        updated_at: NaiveDateTime.local_now,
        id: item["shop_collection_id"]
       ] end)

      # Insert
      Repo.insert_all(Category, entries)

      # Update products categories
      case async do
        true ->
          json["data"]["items"] |> Enum.map(fn x -> Task.start(fn -> update_category_products(x["shop_collection_id"]) end) end)
        false ->
          json["data"]["items"] |> Enum.map(fn x -> Task.start(fn -> update_category_products(x["shop_collection_id"]) end) end)

      end
    rescue
      e in HTTPoison.Error ->
        IO.inspect e
        raise e
    end
  end



  @doc """
  Update category products list
  """
  def update_category_products(category_id) do
    try do
      %HTTPoison.Response{body: body} = HTTPoison.get!(
        "https://shopee.vn/api/v2/search_items/?by=pop&limit=30&match_id=#{@shop_id}&newest=0&order=desc&page_type=shop&shop_categoryids=#{category_id}&version=2")
      json = Jason.decode!(body)

      # Make list of product id
      product_ids = json["items"] |> Enum.map(fn x -> x["itemid"] end)

      # Concat all id to a string
      product_ids_string = Enum.join(product_ids, "|")

      # Fetch current category
      category = Repo.get!(Category, category_id)

      # Update category
      category = Ecto.Changeset.change category, products: product_ids_string
      case Repo.update category do
        {:ok, struct} -> IO.inspect struct
        {:error, changeset} -> IO.inspect.changeset
      end
    rescue
      HTTPoison.Error -> raise "Failed to fetch"
    end
  end


  @doc """
  Fetching all resource
  """
  def fetch_all(async \\ true) do
    case async do
      true ->
          Task.start(fn -> get_categories(true) end)
          Task.start(fn -> get_products() end)
        :ok
      false ->
          get_categories(false)
          get_products()
          # Task.async(fn -> get_products() end) |> Task.await
          # Task.async(fn -> get_categories(false) end) |> Task.await
    end
  end


  @doc """
  Get product image
  """
  def get_product_image(product) do
    "https://cf.shopee.vn/file/#{product.image_id}_tn"
  end
end
